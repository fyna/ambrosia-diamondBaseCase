#!/usr/bin/python

#------------------------------------------------------------------
# This script transforms an openfoam mesh by shifting its point coordinates.


# date: 06/11/2018
# author: Fynn Jerome Aschmoneit
#------------------------------------------------------------------

from __future__ import division		# to allow floating point division
import sys, getopt	# for command line arguments and options
import re
import math

pi		= 3.14159265359

def transPts(cdr, alpha_deg):
    # multiply by 1000
    # a = [c*1e3 for c in cdr]
    
    alpha 	= alpha_deg/180.0*pi 
    x	= cdr[0]
    y	= cdr[1]
    z	= cdr[2]

    if( y != 0.0):
        l	= math.sqrt( 2.0 *y**2.0 *(1.0 -math.cos(pi/2.0 -alpha) ) )*y/abs(y)
    else:
        l	= 0

    x_ 	= x +l *math.cos( pi/4.0 - 0.5*alpha )
    # y_  = y
    y_ 	= y -l *math.sin( pi/4.0 - 0.5*alpha )
    z_ 	= z 


    # stretching in x:
    # x_		= 1.5*x
    # y_		= y
    # z_		= 0.1*z


    return [ x_, y_, z_ ]
    

def findAngle(textFile):
    textContentAr = []
    for line in textFile:
        line = line.translate(None, ';')
        for i,c in enumerate(line.split()):
            textContentAr.append(c)

    for i,s in enumerate(textContentAr):
        if s == 'alpha':
            i_alpha = i+1

    return float(textContentAr[i_alpha])

def main(argv):
    alpha_deg = 90
    opts, args = getopt.getopt(argv,"hp:m:")
    for opt, arg in opts:
        if opt == '-h':
            print 'transformMesh.py -p <pathToMeshPointsFile> -m <mesh settings>'
            sys.exit()
        elif opt == '-p':
            fileIn = open(arg, 'r')
        elif opt == '-m':
            fileMeshSetting = open(arg, 'r')

    alpha_deg = findAngle(fileMeshSetting)

    fileOut 	= open("transPoints", 'w')
    
    for lineNb, line in enumerate(fileIn):
        nbCounter 	= 0
        nbAr		= []
        for c in re.split(' |\(|\)|\n',line):
            try:
                nbAr.append( float(c) ) 
                nbCounter = nbCounter + 1
            except ValueError:
                a=1
        
        if nbCounter == 3:
            crdTrans = transPts(nbAr, alpha_deg)
            fileOut.write( "(" +str(crdTrans[0]) +" " +str(crdTrans[1]) +" " +str(crdTrans[2]) +")\n"  )
        else:
            fileOut.write(line)
            
    fileOut.close()
    fileIn.close()
                
if __name__ == "__main__":
    main(sys.argv[1:])
    
